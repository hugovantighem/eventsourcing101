package projection

import (
	"gitlab.com/hugovantighem/eventsourcing101/domain"
)

type TodoChangesReport struct {
	Items map[string]int
}

func NewTodoChangesReport() TodoChangesReport {
	return TodoChangesReport{
		Items: map[string]int{},
	}
}

func (x *TodoChangesReport) OnTodoCreated(evt domain.TodoCreated) {
	x.Items[evt.AggregateId] = 0
}
func (x *TodoChangesReport) OnTodoCompleted(evt domain.TodoCompleted) error {
	// nothing
	return nil
}
func (x *TodoChangesReport) OnTodoDescriptionUpdated(evt domain.TodoDescriptionUpdated) error {
	x.Items[evt.AggregateId] += 1
	return nil
}
