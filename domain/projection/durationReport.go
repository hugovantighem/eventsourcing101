package projection

import (
	"time"

	"gitlab.com/hugovantighem/eventsourcing101/domain"
)

type DurationReport struct {
	Items map[string]*TodoDuration
}

type TodoDuration struct {
	AggregateId string
	Start       time.Time
	End         time.Time
	Duration    int
}

func NewDurationReport() DurationReport {
	return DurationReport{
		Items: map[string]*TodoDuration{},
	}
}

func (x *DurationReport) OnEvent(event domain.Event) {
	if event.AggregateType != domain.TodoAggregateType {
		return
	}
	switch event.Category {
	case domain.TodoCreatedType:
		x.Items[event.AggregateId] = &TodoDuration{
			AggregateId: event.AggregateId,
			Start:       event.Timestamp,
		}
	case domain.TodoCompletedType:
		x.Items[event.AggregateId].End = event.Timestamp
		x.Items[event.AggregateId].Duration = int(x.Items[event.AggregateId].End.Sub(x.Items[event.AggregateId].Start).Minutes())
	}
}
