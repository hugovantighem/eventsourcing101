package projection

import "gitlab.com/hugovantighem/eventsourcing101/domain"

type TodoProjection interface {
	OnTodoCreated(evt domain.TodoCreated)
	OnTodoCompleted(evt domain.TodoCompleted) error
	OnTodoDescriptionUpdated(evt domain.TodoDescriptionUpdated) error
}

type EventProjection interface {
	OnEvent(evt domain.Event)
}
