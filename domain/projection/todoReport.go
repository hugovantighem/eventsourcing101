package projection

import (
	"gitlab.com/hugovantighem/eventsourcing101/domain"
)

type TodoReport struct {
	TotalCount     int
	ActiveCount    int
	CompletedCount int
}

func (x *TodoReport) OnTodoCreated(evt domain.TodoCreated) {
	x.TotalCount += 1
	x.ActiveCount += 1
}
func (x *TodoReport) OnTodoCompleted(evt domain.TodoCompleted) error {
	x.CompletedCount += 1
	x.ActiveCount -= 1
	return nil
}
func (x *TodoReport) OnTodoDescriptionUpdated(evt domain.TodoDescriptionUpdated) error {
	// nothing
	return nil
}
