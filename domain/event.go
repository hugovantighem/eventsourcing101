package domain

import "time"

type Event struct {
	Id              string
	Category        string
	AggregateType   string
	AggregateId     string
	SequenceNumber  int
	PayloadRevision string
	PayloadVersion  int
	Payload         string
	Timestamp       time.Time
}
