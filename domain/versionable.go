package domain

import "fmt"

type Versionable interface {
	GetId() string
	GetVersion() int
}

type VersionableFields struct {
	AggregateId string
	Version     int
}

func (x VersionableFields) GetId() string {
	return x.AggregateId
}
func (x VersionableFields) GetVersion() int {
	return x.Version
}

func VefifyCompatibility(x, y Versionable) error {
	if x.GetId() != y.GetId() {
		return fmt.Errorf("invalid Id, expecting %s, got %s", x.GetId(), y.GetId())
	}
	if x.GetVersion() != y.GetVersion() {
		return fmt.Errorf("invalid version, current %d, given %d", x.GetVersion(), y.GetVersion())
	}
	return nil
}
