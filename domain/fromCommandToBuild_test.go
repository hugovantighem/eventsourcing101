package domain

import (
	"encoding/json"
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_CreateTodo_then_Build_from_TodoCreate(t *testing.T) {

	// given a command
	cmd := CreateTodoCmd{
		Description: "my first todo",
	}
	// when handling the command
	item, event, err := OnCreateTodo(cmd)

	// then no error is raised
	assert.Nil(t, err)

	// and an item is created
	assert.Equal(t, cmd.Description, item.Description)
	assert.Equal(t, false, item.Completed)

	// and an event is produced
	assert.Equal(t, "TodoCreated", event.Category)
	assert.Equal(t, "Todo", event.AggregateType)

	// given an event
	evt := TodoCreated{}
	err = json.Unmarshal([]byte(event.Payload), &evt)
	assert.Nil(t, err)
	result := &Todo{}
	// when replaying it
	result.OnTodoCreated(evt)
	// then state is correctly set
	assert.Equal(t, false, result.Completed)
	assert.Equal(t, result.Description, cmd.Description)
	initialVersion := result.Version
	assert.Equal(t, 0, initialVersion)

	// given another event
	completedEvent := TodoCompleted{
		VersionableFields: VersionableFields{
			AggregateId: result.Id,
			Version:     result.Version + 1,
		},
	}
	// when model replays it
	result.OnTodoCompleted(completedEvent)
	// then state is correctly set
	assert.Equal(t, true, result.Completed)
	assert.Equal(t, initialVersion+1, result.Version)
}

func Test_WrongEventAggregateId_raises_an_error(t *testing.T) {
	item := Todo{
		Id:          "1234",
		Description: "hello event sourcing",
	}
	evt := TodoCompleted{
		VersionableFields: VersionableFields{
			AggregateId: "wrongID",
		},
	}
	err := item.OnTodoCompleted(evt)
	assert.NotNil(t, err)

}
func Test_WrongEventAggregateVersion_raises_an_error(t *testing.T) {
	// Given an item
	item := &Todo{
		Id:          "1234",
		Description: "hello event sourcing",
		Version:     4,
	}
	// And a command against a stale version
	wrongVersion := item.Version - 1
	cmd := MarkTodoAsCompletedCmd{
		VersionableFields: VersionableFields{
			AggregateId: "1234",
			Version:     wrongVersion,
		},
	}
	// when executing command
	_, err := item.OnMarkTodoAsCompleted(cmd)
	// then an error is raised
	assert.NotNil(t, err)

}
