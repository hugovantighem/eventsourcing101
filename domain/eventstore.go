package domain

type EventStore interface {
	Save(e Event) error
	GetFrom(seqNum int, count int) ([]Event, error)
	GetForAggregateTypeFrom(seqNum int, count int, aggregateType string) ([]Event, error)
	GetForAggregateFrom(seqNum int, count int, aggregateType string, aggregateId string) ([]Event, error)
	GetForAggregateVersion(aggregateType string, aggregateId string, version int) ([]Event, error)
}
