package domain

import (
	"encoding/json"
	"fmt"
	"time"

	"github.com/google/uuid"
)

const (
	TodoCreatedType            = "TodoCreated"
	TodoCompletedType          = "TodoCompleted"
	TodoDescriptionUpdatedType = "TodoDescriptionUpdated"

	TodoAggregateType = "Todo"
)

type Todo struct {
	Id          string
	Description string
	Completed   bool
	Version     int
}

func (x Todo) GetId() string {
	return x.Id
}

func (x Todo) GetVersion() int {
	return x.Version
}

func InitialTodo() *Todo {
	return &Todo{}
}

type CreateTodoCmd struct {
	Description string
}

type ChangeTodoDescriptionCmd struct {
	VersionableFields
	Description string
}

func (x ChangeTodoDescriptionCmd) GetId() string {
	return x.AggregateId
}

func (x ChangeTodoDescriptionCmd) GetVersion() int {
	return x.Version
}

type MarkTodoAsCompletedCmd struct {
	VersionableFields
}

func (x MarkTodoAsCompletedCmd) GetId() string {
	return x.AggregateId
}

func (x MarkTodoAsCompletedCmd) GetVersion() int {
	return x.Version
}

type TodoCreated struct {
	VersionableFields
	Description string
}

type TodoCompleted struct {
	VersionableFields
}

type TodoDescriptionUpdated struct {
	VersionableFields
	Description string
}

func newTodoId() string {
	return fmt.Sprintf("tod-%s", uuid.NewString())
}

// handle command
func OnCreateTodo(cmd CreateTodoCmd) (*Todo, Event, error) {
	item := Todo{
		Id:          newTodoId(),
		Description: cmd.Description,
		Completed:   false,
		Version:     0,
	}
	evt := TodoCreated{
		VersionableFields: VersionableFields{
			AggregateId: item.Id,
			Version:     item.Version,
		},
		Description: item.Description,
	}

	e, err := wrapDomainEvent(evt, evt.VersionableFields, TodoAggregateType, TodoCreatedType)

	return &item, e, err

}
func (x *Todo) OnMarkTodoAsCompleted(cmd MarkTodoAsCompletedCmd) (Event, error) {
	err := VefifyCompatibility(x, cmd) // TODO remove from domain
	if err != nil {
		return Event{}, err
	}
	x.Completed = true
	x.Version += 1
	evt := TodoCompleted{
		VersionableFields: VersionableFields{
			AggregateId: x.GetId(),
			Version:     x.GetVersion(),
		},
	}

	return wrapDomainEvent(evt, evt.VersionableFields, TodoAggregateType, TodoCompletedType)

}
func (x *Todo) OnChangeTodoDescription(cmd ChangeTodoDescriptionCmd) (Event, error) {
	err := VefifyCompatibility(x, cmd) // TODO remove from domain
	if err != nil {
		return Event{}, err
	}
	x.Version += 1
	x.Description = cmd.Description
	evt := TodoDescriptionUpdated{
		VersionableFields: VersionableFields{
			AggregateId: x.GetId(),
			Version:     x.GetVersion(),
		},
		Description: x.Description,
	}

	return wrapDomainEvent(evt, evt.VersionableFields, TodoAggregateType, TodoDescriptionUpdatedType)
}

func wrapDomainEvent(event interface{}, reference Versionable, aggregateType string, category string) (Event, error) {
	b, err := json.Marshal(event)
	if err != nil {
		return Event{}, err
	}
	e := Event{
		Id:              uuid.NewString(),
		Category:        category,
		AggregateType:   aggregateType,
		AggregateId:     reference.GetId(),
		PayloadRevision: "1.0",
		PayloadVersion:  reference.GetVersion(),
		Payload:         string(b),
		Timestamp:       time.Now(),
	}
	return e, nil

}

// projection

func (x *Todo) OnTodoCreated(evt TodoCreated) {
	x.Id = evt.AggregateId
	x.Description = evt.Description
	x.Completed = false
	x.Version = evt.Version
}

func (x *Todo) OnTodoCompleted(evt TodoCompleted) error {
	if err := x.incomingEventValidation(evt); err != nil {
		return err
	}
	x.Completed = true
	x.Version = evt.Version
	return nil
}

func (x *Todo) OnTodoDescriptionUpdated(evt TodoDescriptionUpdated) error {
	if err := x.incomingEventValidation(evt); err != nil {
		return err
	}
	x.Description = evt.Description
	x.Version = evt.Version
	return nil
}

func (x *Todo) incomingEventValidation(event Versionable) error {
	if event.GetId() != x.GetId() {
		return fmt.Errorf("wrong id: expected %s, given %s", x.GetId(), event.GetId())
	}
	return nil
}
