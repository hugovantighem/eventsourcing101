create table Events (
	Id              varchar(100),
	Category        varchar(100),
	AggregateType   varchar(100),
	AggregateId     varchar(100),
	SequenceNumber  INTEGER PRIMARY KEY,
	PayloadRevision varchar(100),
	PayloadVersion  INTEGER,
	Payload         TEXT,
	Timestamp       DATETIME
)