package application

import (
	"encoding/json"
	"fmt"
	"log"
	"strconv"
	"strings"

	"gitlab.com/hugovantighem/eventsourcing101/domain"
	"gitlab.com/hugovantighem/eventsourcing101/domain/projection"
	"gitlab.com/hugovantighem/eventsourcing101/infra"
)

// Mutations

func AddTodo(payload *string, store domain.EventStore) {

	cmd := domain.CreateTodoCmd{
		Description: *payload,
	}
	item, evt, err := domain.OnCreateTodo(cmd)
	if err != nil {
		panic(err)
	}
	err = store.Save(evt)
	if err != nil {
		panic(err)
	}
	log.Printf("todo %s created", item.Id)

}

func UpdateTodo(payload *string, store domain.EventStore) {
	parts := strings.Split(*payload, ",")

	item, _, err := loadLatestTodo(parts[0], store)
	if err != nil {
		panic(err)
	}
	version, _ := strconv.Atoi(parts[1])
	cmd := domain.ChangeTodoDescriptionCmd{
		VersionableFields: domain.VersionableFields{
			AggregateId: parts[0],
			Version:     version,
		},
		Description: parts[2],
	}
	evt, err := item.OnChangeTodoDescription(cmd)
	if err != nil {
		panic(err)
	}
	err = store.Save(evt)
	if err != nil {
		panic(err)
	}
	log.Printf("todo %s description updated", item.Id)

}

func CompleteTodo(payload *string, store domain.EventStore) {
	parts := strings.Split(*payload, ",")

	item, _, err := loadLatestTodo(parts[0], store)
	if err != nil {
		panic(err)
	}
	version, _ := strconv.Atoi(parts[1])
	cmd := domain.MarkTodoAsCompletedCmd{
		VersionableFields: domain.VersionableFields{
			AggregateId: parts[0],
			Version:     version,
		},
	}
	evt, err := item.OnMarkTodoAsCompleted(cmd)
	if err != nil {
		panic(err)
	}
	err = store.Save(evt)
	if err != nil {
		panic(err)
	}
	log.Printf("todo %s completed", item.Id)

}

// load aggregate
type loadTodoFn func() ([]domain.Event, error)

func loadTodoWithEvents(fn loadTodoFn) (domain.Todo, []domain.Event, error) {

	item := domain.InitialTodo()
	evts, err := fn()
	if err != nil {
		return *item, nil, err
	}
	infra.PlayTodoEvents(item, evts)
	return *item, evts, nil
}

func loadLatestTodo(id string, store domain.EventStore) (domain.Todo, []domain.Event, error) {
	loadTodo := func() ([]domain.Event, error) {
		return store.GetForAggregateFrom(0, 100, domain.TodoAggregateType, id)
	}
	return loadTodoWithEvents(loadTodo)
}

func loadTodoVersion(id string, version int, store domain.EventStore) (domain.Todo, error) {
	loadTodo := func() ([]domain.Event, error) {
		return store.GetForAggregateVersion(domain.TodoAggregateType, id, version)
	}

	item, _, err := loadTodoWithEvents(loadTodo)
	return item, err
}

// Projections

func ListTodos(store domain.EventStore) {

	evts, err := store.GetForAggregateTypeFrom(0, 100, domain.TodoAggregateType)
	if err != nil {
		panic(err)
	}
	items := map[string]*domain.Todo{}
	for _, evt := range evts {
		_, ok := items[evt.AggregateId]
		if !ok {
			items[evt.AggregateId] = domain.InitialTodo()
		}
		item := items[evt.AggregateId]
		infra.PlayTodoEvents(item, []domain.Event{evt})
	}
	for _, item := range items {
		log.Println(item.Id, item.Version, item.Description)
	}
}

func TodoReport(store domain.EventStore) {
	proj := projection.TodoReport{}
	evts, err := store.GetForAggregateTypeFrom(0, 100, domain.TodoAggregateType)
	if err != nil {
		panic(err)
	}
	infra.PlayTodoEvents(&proj, evts)
	b, err := json.Marshal(proj)
	if err != nil {
		panic(err)
	}
	log.Println(string(b))
}

func TodoDetail(payload *string, store domain.EventStore) {
	item, events, err := loadLatestTodo(*payload, store)
	if err != nil {
		panic(err)
	}
	log.Println(item.Id, item.Description, fmt.Sprintf("completed %t", item.Completed), fmt.Sprintf("version %d", item.Version))
	for _, ev := range events {
		log.Println(ev.Category, ev.PayloadVersion)
	}
}

func TodoVersion(payload *string, store domain.EventStore) {
	parts := strings.Split(*payload, ",")
	version, _ := strconv.Atoi(parts[1])
	item, err := loadTodoVersion(parts[0], version, store)
	if err != nil {
		panic(err)
	}
	log.Println(item.Id, item.Description, fmt.Sprintf("completed %t", item.Completed), fmt.Sprintf("version %d", item.Version))

}

func TodoChanges(store domain.EventStore) {
	proj := projection.NewTodoChangesReport()
	evts, err := store.GetForAggregateTypeFrom(0, 100, domain.TodoAggregateType)
	if err != nil {
		panic(err)
	}
	infra.PlayTodoEvents(&proj, evts)
	b, err := json.Marshal(proj)
	if err != nil {
		panic(err)
	}
	log.Println(string(b))
}

func DurationReport(store domain.EventStore) {
	proj := projection.NewDurationReport()
	evts, err := store.GetForAggregateTypeFrom(0, 100, domain.TodoAggregateType)
	if err != nil {
		panic(err)
	}
	infra.PlayEvents(&proj, evts)
	b, err := json.Marshal(proj)
	if err != nil {
		panic(err)
	}
	log.Println(string(b))

}
