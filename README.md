# eventSourcing101


Project used to explore Event Sourcing.

- event modeling
- command modeling
- event store
- build aggregate from events
- projections
- updates on domain model
- updates on events
- optimistic locking
- GDPR
