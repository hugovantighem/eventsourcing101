package main

import (
	"database/sql"
	"encoding/json"
	"log"
	"time"

	"github.com/google/uuid"
	_ "github.com/mattn/go-sqlite3"
	"gitlab.com/hugovantighem/eventsourcing101/domain"
	"gitlab.com/hugovantighem/eventsourcing101/infra"
)

func main() {
	db, err := sql.Open("sqlite3", "./es.db")
	if err != nil {
		panic(err)
	}
	var store domain.EventStore = &infra.EventStore{Db: db}
	insert(store)
	if err != nil {
		panic(err)
	}
	items, err := store.GetForAggregateFrom(
		1,
		10,
		"User",
		"2840c09b-3d81-4aa9-a064-fad9dab2214c",
	)
	if err != nil {
		panic(err)
	}
	for _, item := range items {
		b, err := json.MarshalIndent(item, "", " ")
		if err != nil {
			panic(err)
		}
		log.Println(string(b))
	}
}

func insert(store domain.EventStore) error {
	evt := domain.Event{
		Id:              uuid.NewString(),
		Category:        "UserCreated",
		AggregateType:   "User",
		AggregateId:     uuid.NewString(),
		PayloadRevision: "1.0",
		PayloadVersion:  1,
		Payload:         "{\"name\":\"foo\"}",
		Timestamp:       time.Now(),
	}
	return store.Save(evt)
}
