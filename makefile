create_db:
	sqlite3 es.db < scripts/db.sql

tests_unit:
	go test ./domain/... ./infra/...

cmd_add:
	go run cmd/cli/main.go -cmd=add -payload="desc"

cmd_list:
	go run cmd/cli/main.go -cmd=list

cmd_detail:
	go run cmd/cli/main.go -cmd=detail -payload="todoID"

cmd_detail_version:
	go run cmd/cli/main.go -cmd=version -payload="todoID,version"

cmd_complete:
	go run cmd/cli/main.go -cmd=complete -payload="todoID,version"

cmd_update:
	go run cmd/cli/main.go -cmd=update -payload="todoID,version,desc"

cmd_report:
	go run cmd/cli/main.go -cmd=report
	
cmd_report_changes:
	go run cmd/cli/main.go -cmd=report_changes

cmd_report_duration:
	go run cmd/cli/main.go -cmd=report_duration