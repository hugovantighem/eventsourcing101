package infra

import (
	"database/sql"
	"fmt"
	"log"

	"gitlab.com/hugovantighem/eventsourcing101/domain"
)

type EventStore struct {
	Db *sql.DB
}

func (x *EventStore) Save(e domain.Event) error {
	return insert(x.Db, e)
}
func (x *EventStore) GetFrom(seqNum int, count int) ([]domain.Event, error) {
	result := []domain.Event{}
	return result, fmt.Errorf("not implemented")
}
func (x *EventStore) GetForAggregateTypeFrom(seqNum int, count int, aggregateType string) ([]domain.Event, error) {
	return selectEventsForType(x.Db, seqNum, count, aggregateType)
}
func (x *EventStore) GetForAggregateFrom(seqNum int, count int, aggregateType string, aggregateId string) ([]domain.Event, error) {
	return selectEventsForAggregate(x.Db, seqNum, count, aggregateType, aggregateId)
}
func (x *EventStore) GetForAggregateVersion(aggregateType string, aggregateId string, version int) ([]domain.Event, error) {
	return selectEventsForAggregateVersion(x.Db, aggregateType, aggregateId, version)
}

func selectEventsForType(db *sql.DB, seqNum int, count int, aggregateType string) ([]domain.Event, error) {

	rows, err := db.Query(SELECT_FROM+`
	where SequenceNumber >= ?
	and AggregateType = ?
	order by SequenceNumber asc 
	limit ?`,
		seqNum,
		aggregateType,
		count)

	if err != nil {
		return nil, err
	}

	result, err := mapRowsToEvents(rows)
	if err != nil {
		return nil, err
	}
	return result, nil
}

func mapRowsToEvents(rows *sql.Rows) ([]domain.Event, error) {
	result := []domain.Event{}

	for rows.Next() {
		evt := domain.Event{}
		err := rows.Scan(
			&evt.Id,
			&evt.Category,
			&evt.AggregateType,
			&evt.AggregateId,
			&evt.SequenceNumber,
			&evt.PayloadRevision,
			&evt.PayloadVersion,
			&evt.Payload,
			&evt.Timestamp,
		)
		if err != nil {
			return nil, err
		}
		result = append(result, evt)
	}
	rows.Close()

	return result, nil
}

func selectEventsForAggregate(db *sql.DB, seqNum int, count int, aggregateType string, aggregateId string) ([]domain.Event, error) {

	rows, err := db.Query(SELECT_FROM+`
	where SequenceNumber >= ?
	and AggregateType = ?
	and AggregateId = ?
	order by SequenceNumber asc 
	limit ?`,
		seqNum,
		aggregateType,
		aggregateId,
		count)
	if err != nil {
		return nil, err
	}
	result, err := mapRowsToEvents(rows)
	if err != nil {
		return nil, err
	}
	return result, nil
}

func selectEventsForAggregateVersion(db *sql.DB, aggregateType string, aggregateId string, version int) ([]domain.Event, error) {

	rows, err := db.Query(SELECT_FROM+` 
	where AggregateType = ?
	and AggregateId = ?
	and PayloadVersion <= ?
	order by SequenceNumber asc `,
		aggregateType,
		aggregateId,
		version)
	if err != nil {
		return nil, err
	}
	result, err := mapRowsToEvents(rows)
	if err != nil {
		return nil, err
	}
	return result, nil
}

const SELECT_FROM = `SELECT 
Id,
Category,
AggregateType,
AggregateId,
SequenceNumber,
PayloadRevision,
PayloadVersion,
Payload,
Timestamp 
FROM events `

const INSERT_STMT = `
	INSERT INTO events(Id,
		Category,
		AggregateType,
		AggregateId,
		PayloadRevision,
		PayloadVersion,
		Payload,
		Timestamp) 
		values( ?,?,?,?,?,?,?,? )`

func insert(db *sql.DB, e domain.Event) error {

	stmt, err := db.Prepare(INSERT_STMT)
	if err != nil {
		return err
	}

	res, err := stmt.Exec(
		e.Id,
		e.Category,
		e.AggregateType,
		e.AggregateId,
		e.PayloadVersion,
		e.PayloadVersion,
		e.Payload,
		e.Timestamp,
	)
	if err != nil {
		return err
	}
	affect, err := res.RowsAffected()
	if err != nil {
		return err
	}
	log.Printf("%d rows affected", affect)
	return nil
}
