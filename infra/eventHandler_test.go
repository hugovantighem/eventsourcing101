package infra

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/hugovantighem/eventsourcing101/domain"
)

func Test_eventHandler(t *testing.T) {
	// given events
	events := []domain.Event{}

	cmd := domain.CreateTodoCmd{
		Description: "hello ES",
	}
	// when handling the command
	item, event, _ := domain.OnCreateTodo(cmd)
	events = append(events, event)

	cmd2 := domain.MarkTodoAsCompletedCmd{
		VersionableFields: domain.VersionableFields{
			AggregateId: item.Id,
		},
	}

	evt2, _ := item.OnMarkTodoAsCompleted(cmd2)
	events = append(events, evt2)

	// when loading model
	result := domain.InitialTodo()
	err := PlayTodoEvents(result, events)
	// then state is correctly set
	assert.Equal(t, true, result.Completed)
	assert.Equal(t, cmd.Description, result.Description)
	// and no error is raised
	assert.Nil(t, err)
}
