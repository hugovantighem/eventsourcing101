package infra

import (
	"encoding/json"

	"gitlab.com/hugovantighem/eventsourcing101/domain"
	"gitlab.com/hugovantighem/eventsourcing101/domain/projection"
)

// TODO move to application
func PlayTodoEvents(item projection.TodoProjection, events []domain.Event) error {
	for _, event := range events {
		switch event.Category {

		case domain.TodoCreatedType:
			evt := domain.TodoCreated{}
			UnmarshalEvent(event.Payload, &evt)
			item.OnTodoCreated(evt)

		case domain.TodoCompletedType:
			evt := domain.TodoCompleted{}
			UnmarshalEvent(event.Payload, &evt)
			if err := item.OnTodoCompleted(evt); err != nil {
				return err
			}

		case domain.TodoDescriptionUpdatedType:
			evt := domain.TodoDescriptionUpdated{}
			UnmarshalEvent(event.Payload, &evt)
			if err := item.OnTodoDescriptionUpdated(evt); err != nil {
				return err
			}
		}
	}
	return nil
}

func PlayEvents(proj projection.EventProjection, events []domain.Event) {
	for _, event := range events {
		proj.OnEvent(event)
	}
}

func UnmarshalEvent(payload string, event interface{}) {
	err := json.Unmarshal([]byte(payload), event)
	if err != nil {
		panic(err)
	}
}
