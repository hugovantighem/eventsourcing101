package main

import (
	"database/sql"
	"flag"
	"log"

	_ "github.com/mattn/go-sqlite3"
	"gitlab.com/hugovantighem/eventsourcing101/application"
	"gitlab.com/hugovantighem/eventsourcing101/domain"
	"gitlab.com/hugovantighem/eventsourcing101/infra"
)

const (
	CMD_LIST            = "list"
	CMD_ADD             = "add"
	CMD_DETAIL          = "detail"
	CMD_UPDATE          = "update"
	CMD_COMPLETE        = "complete"
	CMD_VERSION         = "version"
	CMD_REPORT          = "report"
	CMD_REPORT_CHANGES  = "report_changes"
	CMD_REPORT_DURATION = "report_duration"
)

func main() {

	cmd := flag.String("cmd", CMD_LIST, "list all todos")
	payload := flag.String("payload", "", "command payload")

	flag.Parse()
	log.Println(*cmd)
	store := getStore()

	switch *cmd {
	case CMD_LIST:
		application.ListTodos(store)
	case CMD_ADD:
		application.AddTodo(payload, store)
	case CMD_DETAIL:
		application.TodoDetail(payload, store)
	case CMD_COMPLETE:
		application.CompleteTodo(payload, store)
	case CMD_UPDATE:
		application.UpdateTodo(payload, store)
	case CMD_VERSION:
		application.TodoVersion(payload, store)
	case CMD_REPORT:
		application.TodoReport(store)
	case CMD_REPORT_CHANGES:
		application.TodoChanges(store)
	case CMD_REPORT_DURATION:
		application.DurationReport(store)
	}
}

func getStore() domain.EventStore {
	db, err := sql.Open("sqlite3", "es.db")
	if err != nil {
		panic(err)
	}
	return &infra.EventStore{Db: db}
}
